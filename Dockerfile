from ubuntu:20.04

RUN apt-get -y update && apt-get -y upgrade
RUN apt-get -y install openjdk-11-jre-headless
RUN apt-get -y install postgresql
RUN apt-get -y install unzip curl
RUN apt-get -y install tomcat9


# Download and unpack AuditConsole WAR archive
#
RUN curl -o /tmp/AuditConsole.war https://auditconsole.com/downloads/AuditConsole-0.4.8pre1.war
RUN mkdir -p /usr/share/tomcat9/webapps/ROOT
RUN unzip -d /usr/share/tomcat9/webapps/ROOT /tmp/AuditConsole.war
RUN rm /tmp/AuditConsole.war


# Add missing link for Tomcat9 configuration directory (ubuntu bug?)
#
RUN ln -s /usr/share/tomcat9/etc /usr/share/tomcat9/conf


# Start postgres and create 'console' users + auditconsoledb database.
#
USER postgres
RUN /etc/init.d/postgresql start &&\
    psql --command "CREATE USER console WITH SUPERUSER PASSWORD 'console';" &&\
    createdb -O console auditconsoledb

RUN echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/12/main/pg_hba.conf
RUN echo "listen_addresses='*'" >> /etc/postgresql/12/main/postgresql.conf


# Create console user, AuditConsole base-directory
#
USER root
RUN mkdir -p /opt/AuditConsole
RUN adduser --home /opt/AuditConsole console
RUN chown -R console /opt/AuditConsole

COPY run.sh /run.sh
RUN chmod 755 /run.sh

ENTRYPOINT ["/run.sh"]