# Dockerfile for AuditConsole

This docker file creates a container with a recent Java 11 runtime and
a recent version of the AuditConsole.

The container is based in Ubuntu:20.04 with the following packages:

  - OpenJDK 11 JRE (headless)
  - Apache Tomcat 9 (latest Ubuntu package version)
  - A PostgreSQL database.


The PostgreSQL database is created during container build with the name `auditconsoledb`
and user `console` with pasword `console`.


## Building the Container

The docker container can be built with

     # docker build -t auditconsole:1 .

where `auditconsole:1` is the resulting docker image name.


## Running the AuditConsole in Docker

After building the container, the AuditConsole can be started with

     # docker run -ti -p 8080:8080 auditconsole:1

where `8080` is the port at which the AuditConsole application server is listening.


## Setup with PostgreSQL Database

The container image already contains a running PostgreSQL database.
To set up the AuditConsole for using that database use the following properties
in the AuditConsole storage setup dialog:

   - Database URL: `jdbc:postgresql://localhost:5432/auditconsoledb`
   - Database User: `console`
   - Database Password: `console`