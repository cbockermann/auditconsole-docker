#!/bin/bash


# Start the PostgreSQL database
#
/etc/init.d/postgresql start


# Set the default base directory for the AuditConsole
#
export console.home="/opt/AuditConsole"


# run Tomcat9 in foreground mode
/usr/share/tomcat9/bin/catalina.sh run